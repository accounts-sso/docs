var namespaceSignOn =
[
    [ "AuthService", "classSignOn_1_1AuthService.html", "classSignOn_1_1AuthService" ],
    [ "AuthSession", "classSignOn_1_1AuthSession.html", "classSignOn_1_1AuthSession" ],
    [ "DBusInterface", "classSignOn_1_1DBusInterface.html", "classSignOn_1_1DBusInterface" ],
    [ "Identity", "classSignOn_1_1Identity.html", "classSignOn_1_1Identity" ],
    [ "IdentityInfo", "classSignOn_1_1IdentityInfo.html", "classSignOn_1_1IdentityInfo" ],
    [ "SessionData", "classSignOn_1_1SessionData.html", "classSignOn_1_1SessionData" ],
    [ "Error", "classSignOn_1_1Error.html", "classSignOn_1_1Error" ]
];