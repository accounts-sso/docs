var classSignOn_1_1AuthService =
[
    [ "IdentityRegExp", "classSignOn_1_1AuthService_1_1IdentityRegExp.html", "classSignOn_1_1AuthService_1_1IdentityRegExp" ],
    [ "IdentityFilter", "classSignOn_1_1AuthService.html#a3acba53f72b72ef9b08ea8a4d49dd344", null ],
    [ "IdentityFilterCriteria", "classSignOn_1_1AuthService.html#ad9ac832e70cb6251375e9947d558d2b5", [
      [ "AuthMethod", "classSignOn_1_1AuthService.html#ad9ac832e70cb6251375e9947d558d2b5a5b4e2c7214a1788ebc89afbbd53d51c3", null ],
      [ "Username", "classSignOn_1_1AuthService.html#ad9ac832e70cb6251375e9947d558d2b5a8440612cab411aa71006b62fad4d2022", null ],
      [ "Realm", "classSignOn_1_1AuthService.html#ad9ac832e70cb6251375e9947d558d2b5a1eadce631a4c4009c1e6cd615f859049", null ],
      [ "Caption", "classSignOn_1_1AuthService.html#ad9ac832e70cb6251375e9947d558d2b5aafcf56ac5c47a75b6b98acaf9f145e56", null ]
    ] ],
    [ "ServiceError", "classSignOn_1_1AuthService.html#a95926b7896f59c4b95dd0d761283e674", [
      [ "UnknownError", "classSignOn_1_1AuthService.html#a95926b7896f59c4b95dd0d761283e674a061c94a1bda6a9dc52f3db06416f8ea4", null ],
      [ "InternalServerError", "classSignOn_1_1AuthService.html#a95926b7896f59c4b95dd0d761283e674ad9b74db38dcc0d46704ac1744ab5d376", null ],
      [ "InternalCommunicationError", "classSignOn_1_1AuthService.html#a95926b7896f59c4b95dd0d761283e674a8ee12cca21786aef26458a5d97fa6443", null ],
      [ "PermissionDeniedError", "classSignOn_1_1AuthService.html#a95926b7896f59c4b95dd0d761283e674a7706acb68a8a28407081be4f95fb0fa7", null ],
      [ "AuthServiceErr", "classSignOn_1_1AuthService.html#a95926b7896f59c4b95dd0d761283e674a111b0c7c62389f525a0c04536b8553a3", null ],
      [ "MethodNotKnownError", "classSignOn_1_1AuthService.html#a95926b7896f59c4b95dd0d761283e674a615eccb98ad8bc91f732c9301a8f8478", null ],
      [ "NotAvailableError", "classSignOn_1_1AuthService.html#a95926b7896f59c4b95dd0d761283e674a23340e9d74be43a5294910ac883e1c1c", null ],
      [ "InvalidQueryError", "classSignOn_1_1AuthService.html#a95926b7896f59c4b95dd0d761283e674a52d38ac87577622827a731c9df88b957", null ]
    ] ],
    [ "AuthService", "classSignOn_1_1AuthService.html#a748d77ab6c54eebc10fdc2b8eaea9d09", null ],
    [ "~AuthService", "classSignOn_1_1AuthService.html#a6ec8f4adfee773d7c87d3df313bdd8e2", null ],
    [ "clear", "classSignOn_1_1AuthService.html#a6e4ae4f8b9ae3dadb188de6a8b618593", null ],
    [ "cleared", "classSignOn_1_1AuthService.html#a8d8546a22f703cfd6cfa78f38fe5ffd0", null ],
    [ "error", "classSignOn_1_1AuthService.html#aac62087bd1ba2f14a110c5e6198b72c6", null ],
    [ "identities", "classSignOn_1_1AuthService.html#aa5ea718192b2b64ae101d6786c36754f", null ],
    [ "mechanismsAvailable", "classSignOn_1_1AuthService.html#a9a8d832cfa6556d5bfbb9ffb91b5745b", null ],
    [ "methodsAvailable", "classSignOn_1_1AuthService.html#ac6c9693273b4548b7cc0c47a438c0c5c", null ],
    [ "queryIdentities", "classSignOn_1_1AuthService.html#a7925066f708d4db50e0bbf8cdc527ba0", null ],
    [ "queryMechanisms", "classSignOn_1_1AuthService.html#ac8b2fc67f4309f88582e22116825b38a", null ],
    [ "queryMethods", "classSignOn_1_1AuthService.html#a42fc5513999da38613815e801b3e1565", null ],
    [ "AuthServiceImpl", "classSignOn_1_1AuthService.html#a922629f82bf215fa8ec660d1a46fbc6f", null ]
];