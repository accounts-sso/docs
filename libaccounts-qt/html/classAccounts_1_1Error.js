var classAccounts_1_1Error =
[
    [ "ErrorType", "classAccounts_1_1Error.html#ab0df38968e4f03a3f1f6d6df0f31f45a", [
      [ "NoError", "classAccounts_1_1Error.html#ab0df38968e4f03a3f1f6d6df0f31f45aaef9104c292609ba6db320509be8fe27f", null ],
      [ "Unknown", "classAccounts_1_1Error.html#ab0df38968e4f03a3f1f6d6df0f31f45aa4e81c184ac3ad48a389cd4454c4a05bb", null ],
      [ "Database", "classAccounts_1_1Error.html#ab0df38968e4f03a3f1f6d6df0f31f45aa31884bb2cfbd4d8e2d428904eb1c3f98", null ],
      [ "Deleted", "classAccounts_1_1Error.html#ab0df38968e4f03a3f1f6d6df0f31f45aa04ba35ff69a05b2a16733a01fc003d88", null ],
      [ "DatabaseLocked", "classAccounts_1_1Error.html#ab0df38968e4f03a3f1f6d6df0f31f45aa155f5bb6520a1d872efe0563428315bf", null ],
      [ "AccountNotFound", "classAccounts_1_1Error.html#ab0df38968e4f03a3f1f6d6df0f31f45aab49b276755b64c7a63a391e03aebbf49", null ]
    ] ],
    [ "Error", "classAccounts_1_1Error.html#a17be1abe802fb9ab3acebe900748cf79", null ],
    [ "Error", "classAccounts_1_1Error.html#abc095ef325fbb7c22399270e62f400ca", null ],
    [ "Error", "classAccounts_1_1Error.html#a43fd5cb964997186acb7f0297cefd666", null ],
    [ "~Error", "classAccounts_1_1Error.html#a810251c55fc575f642cf343c4413c2b1", null ],
    [ "message", "classAccounts_1_1Error.html#aba2e3009745c37baeaf086e1bc6a3b8d", null ],
    [ "operator=", "classAccounts_1_1Error.html#a25f9ffea919d1d0c265a93df49ee3b32", null ],
    [ "type", "classAccounts_1_1Error.html#ac3b58ce6dc3ba4cbfbabd9d7d7774567", null ]
];