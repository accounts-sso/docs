var annotated =
[
    [ "Accounts", null, [
      [ "AccountService", "classAccounts_1_1AccountService.html", "classAccounts_1_1AccountService" ],
      [ "Watch", "classAccounts_1_1Watch.html", "classAccounts_1_1Watch" ],
      [ "Account", "classAccounts_1_1Account.html", "classAccounts_1_1Account" ],
      [ "Application", "classAccounts_1_1Application.html", "classAccounts_1_1Application" ],
      [ "AuthData", "classAccounts_1_1AuthData.html", "classAccounts_1_1AuthData" ],
      [ "Error", "classAccounts_1_1Error.html", "classAccounts_1_1Error" ],
      [ "Manager", "classAccounts_1_1Manager.html", "classAccounts_1_1Manager" ],
      [ "Provider", "classAccounts_1_1Provider.html", "classAccounts_1_1Provider" ],
      [ "ServiceType", "classAccounts_1_1ServiceType.html", "classAccounts_1_1ServiceType" ],
      [ "Service", "classAccounts_1_1Service.html", "classAccounts_1_1Service" ]
    ] ]
];